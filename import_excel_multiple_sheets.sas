/* Generated Code (IMPORT) */
/* Source File: products.xls */
/* Source Path: /folders/myfolders/sas_code/data */
/* Code generated on: 2/11/19, 10:28 PM */


FILENAME REFFILE '/folders/myfolders/sas_code/data/products.xls';

%let sheet=Children;
%web_drop_table(WORK.&sheet);
PROC IMPORT DATAFILE=REFFILE
	DBMS=XLS
	OUT=WORK.&sheet;
	GETNAMES=YES;
	SHEET="&sheet";
RUN;
PROC CONTENTS DATA=WORK.&sheet; RUN;
%web_open_table(WORK.&sheet);

%let sheet=Outdoors;
%web_drop_table(WORK.&sheet);

PROC IMPORT DATAFILE=REFFILE
	DBMS=XLS
	OUT=WORK.&sheet;
	GETNAMES=YES;
	SHEET="&sheet";
RUN;
PROC CONTENTS DATA=WORK.&sheet; RUN;
%web_open_table(WORK.&sheet);

%let sheet=ClothesShoes;
%web_drop_table(WORK.&sheet);

PROC IMPORT DATAFILE=REFFILE
	DBMS=XLS
	OUT=WORK.&sheet;
	GETNAMES=YES;
	SHEET="&sheet";
RUN;
PROC CONTENTS DATA=WORK.&sheet; RUN;
%web_open_table(WORK.&sheet);

%let sheet=Sports;
%web_drop_table(WORK.&sheet);

PROC IMPORT DATAFILE=REFFILE
	DBMS=XLS
	OUT=WORK.&sheet;
	GETNAMES=YES;
	SHEET="&sheet";
RUN;
PROC CONTENTS DATA=WORK.&sheet; RUN;
%web_open_table(WORK.&sheet);

/* Define a Macro program to import each sheet of an Excel file */
%macro import_multiple_sheets(fileref=,sheet=,outlib=WORK);

	%let output=&outlib..&sheet;
	%put NOTE: importing Excel sheet &sheet to &output;
	
	%web_drop_table(&output);
	
	PROC IMPORT DATAFILE=&fileref
		DBMS=XLS
		OUT=&output;
		GETNAMES=YES;
		SHEET="&sheet";
	RUN;
	
	PROC CONTENTS DATA=&output;
	RUN;
	
	%web_drop_table(&output);
%mend;

FILENAME REFFILE '/folders/myfolders/sas_code/data/products.xls';
%import_multiple_sheets(fileref=REFFILE,sheet=Children);
%import_multiple_sheets(fileref=REFFILE,sheet=Outdoors);
%import_multiple_sheets(fileref=REFFILE,sheet=ClothesShoes);
%import_multiple_sheets(fileref=REFFILE,sheet=Sports);

/* SAS XLSX Engine supports multiple Worksheets in Excel XLSX format */
libname XLSLIB XLSX '/folders/myfolders/sas_code/data/products_new.xlsx';
proc copy in=XLSLIB out=WORK;
run;
quit;
