/* Generated Code (IMPORT) */
/* Source File: sales.xls */
/* Source Path: /folders/myfolders/sas_code/data */
/* Code generated on: 2/8/19, 11:55 PM */

%web_drop_table(WORK.IMPORT);


FILENAME REFFILE '/folders/myfolders/sas_code/data/sales.xls';

PROC IMPORT DATAFILE=REFFILE
	DBMS=XLS
	OUT=WORK.IMPORT;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.IMPORT; RUN;


%web_open_table(WORK.IMPORT);